//
//  ColumnsViewController.m
//  Rich Text Demo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "ColumnsViewController.h"
#import "ColumnView.h"
#import <CoreText/CoreText.h>
@interface ColumnsViewController ()
{
    
}
@end

@implementation ColumnsViewController
static NSString * kLipsum;
+ (void) initialize
{
    NSLog(@"%s",__func__);
    NSString  * path = [[NSBundle mainBundle] pathForResource:@"Lipsum" ofType:@"txt"];
    kLipsum = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",kLipsum);
    // Do any additional setup after loading the view.
    CTTextAlignment kAliment = kCTJustifiedTextAlignment;
    CTParagraphStyleSetting paragraphSettings[] = {{kCTParagraphStyleSpecifierAlignment,sizeof(kAliment),&kAliment}};
    id style = (__bridge_transfer id)CTParagraphStyleCreate(paragraphSettings, sizeof(paragraphSettings));
    id key = (__bridge id)kCTParagraphStyleAttributeName;
    NSDictionary * attributes = @{key:style};
    NSAttributedString * attrString = [[NSAttributedString alloc] initWithString:kLipsum attributes:attributes];
    ColumnView * columnView = [[ColumnView alloc] initWithFrame:self.view.bounds];
    columnView.attributedString = attrString;
    [self.view addSubview:columnView];
}

 

@end
