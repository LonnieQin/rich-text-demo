//
//  BeBoldViewController.m
//  Rich Text Demo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "BeBoldViewController.h"

@interface BeBoldViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation BeBoldViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString * string = @"Be Bold!And A little color wouldn't hurt either.";
    NSDictionary * attrs = @{
                            NSFontAttributeName:[UIFont systemFontOfSize:36]
                            };
    NSMutableAttributedString * as = [[NSMutableAttributedString alloc] initWithString:string attributes:attrs];
    [as addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:36] range:[string rangeOfString:@"Bold!"]];
    [as addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:[string rangeOfString:@"little color"]];
    [as addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:[string rangeOfString:@"little"]];
    [as addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Papyrus" size:36] range:[string rangeOfString:@"color"]];
    self.label.attributedText = as;
}
- (IBAction)toggleItalic:(id)sender {
    __block NSMutableAttributedString * as = [self.label.attributedText mutableCopy];
    [as enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, as.length) options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(id value, NSRange range, BOOL *stop) {
        NSLog(@"%@ %@",value,[NSValue valueWithRange:range]);
        UIFont * font = value;
        UIFontDescriptor * descriptor = font.fontDescriptor;
        UIFontDescriptorSymbolicTraits traits = descriptor.symbolicTraits ^ UIFontDescriptorTraitItalic;
        UIFontDescriptor * toggleDescriptor = [descriptor fontDescriptorWithSymbolicTraits:traits];
        UIFont * italicFont = [UIFont fontWithDescriptor:toggleDescriptor size:18];
        NSLog(@"ITALIC %@ %d %d",italicFont,descriptor.symbolicTraits,traits);
        
        
        }];
    self.label.attributedText = as;
}


@end
