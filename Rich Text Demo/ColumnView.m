//
//  ColumnView.m
//  Rich Text Demo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "ColumnView.h"
@interface ColumnView()
@property (nonatomic,readwrite,assign) CFIndex mode;
@end
@implementation ColumnView
- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGAffineTransform transform = CGAffineTransformMakeScale(1, -1);
        CGAffineTransformTranslate(transform, 0, -self.bounds.size.height);
        self.transform = transform;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void) drawRect:(CGRect)rect
{
    if (self.attributedString == nil) {
        return;
    }
    
    
}

@end
