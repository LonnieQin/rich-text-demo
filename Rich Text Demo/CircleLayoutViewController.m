//
//  CircleLayoutViewController.m
//  Rich Text Demo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "CircleLayoutViewController.h"
#import "CircleTextContainer.h"
@interface CircleLayoutViewController ()
{
    UITextView * textView;
    NSString * string;
}
@end

@implementation CircleLayoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString * path = [[NSBundle mainBundle] pathForResource:@"sample.txt" ofType:nil];
    string = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSMutableParagraphStyle * style = [NSMutableParagraphStyle new];
    style.alignment = NSTextAlignmentJustified;
    NSTextStorage * text = [[NSTextStorage alloc] initWithString:string attributes:@{
                                                                                    NSParagraphStyleAttributeName:style,
                                                                                    NSFontAttributeName:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption2]
                                                                                    }];
    NSLayoutManager * layoutManager = [NSLayoutManager new];
    [text  addLayoutManager:layoutManager];
    
    CGRect textViewFrame = self.view.bounds;
    CircleTextContainer * textContainer = [[CircleTextContainer alloc] initWithSize:textViewFrame.size];
    [textContainer setExclusionPaths:@[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(CGRectGetMidX(textViewFrame), CGRectGetMidY(textViewFrame), 150,150)]]];
    [layoutManager addTextContainer:textContainer];
    textView = [[UITextView alloc] initWithFrame:textViewFrame textContainer:textContainer];
    textView.allowsEditingTextAttributes = YES;
    [self.view addSubview:textView];
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotateHandler:) name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void) rotateHandler:(NSNotification*) notification
{
 
}


@end
